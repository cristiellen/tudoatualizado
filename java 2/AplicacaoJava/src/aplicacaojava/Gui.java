
package aplicacaojava;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Gui 
{
    //Atualizado
  
    private JFrame janela;
    private JButton botaoSair;
    private JPanel painelBotoes;
    private JPanel painelBotoes2;
    private JPanel painelGeral;
    private JTextField tela1;
    private JTextField tela2;
    private JLabel clickX;
    private JLabel clickY;
    private JTextField tela3;
    private JLabel posicaoX;
     private JTextField tela4;
    private JLabel posicaoY;
    
    
    

    
    public void construir()
    {
        janela= new JFrame("Java Swing- Manual");
        janela.setBounds(750, 300, 700, 180);
        janela.getContentPane().setLayout(new FlowLayout()); 
       
        painelBotoes = new JPanel(new GridLayout(1, 2));
        painelBotoes2 = new JPanel(new GridLayout(1, 2));
        painelGeral = new JPanel(new GridLayout(2, 1));
        tela1 = new JTextField();
        tela2 = new JTextField();
        tela3 = new JTextField();
        tela4 = new JTextField();
        clickX = new JLabel("X-CLICK: ");
        clickY = new JLabel("Y-CLICK: "); 
        posicaoX = new JLabel("X-POSIÇÃO: "); 
        posicaoY = new JLabel("Y-POSIÇÃO: "); 
       
         tela1.setPreferredSize(new Dimension(90,60));
         tela2.setPreferredSize(new Dimension(90,60));
         clickX.setPreferredSize(new Dimension(90,60));
         posicaoX.setPreferredSize(new Dimension(90,60));
         tela3.setPreferredSize(new Dimension(90,60));
         posicaoY.setPreferredSize(new Dimension(90,60));
         tela4.setPreferredSize(new Dimension(90,60));
          
       
          
          painelBotoes.add(clickX);
          painelBotoes.add(tela1);// colocar o painel dos botoes dentro do contentPane
          painelBotoes.add(clickY);
          painelBotoes.add(tela2);
          painelBotoes2.add(posicaoX);
          painelBotoes2.add(tela3);
          painelBotoes2.add(posicaoY);
          painelBotoes2.add(tela4);
          painelGeral.add(painelBotoes);
          painelGeral.add(painelBotoes2);
        
          painelGeral.add(painelBotoes2);
          janela.getContentPane().add(painelGeral);
         //  janela.getContentPane().add(textos, BorderLayout.CENTER);
        //janela.getContentPane().setSize(100, 10);
        
          tela1.setBackground(Color.WHITE);
          tela2.setBackground(Color.WHITE);
          tela3.setBackground(Color.WHITE);
          tela4.setBackground(Color.WHITE);
          
          tela1.setEditable(false);
          tela2.setEditable(false);
          tela3.setEditable(false);
          tela4.setEditable(false);

        /*botaoSoma = new JButton("+");
        janela.getContentPane().add(botaoSoma, BorderLayout.WEST);
        
        botaoSubtracao = new JButton("-");
        janela.getContentPane().add(botaoSubtracao, BorderLayout.EAST);
        
        botaoSair = new JButton("SAIR");
        janela.getContentPane().add(botaoSair, BorderLayout.SOUTH);*/
        
        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        acoesDosBotoes();
        
        janela.setVisible(true);  
    }
   
    
    public void acoesDosBotoes()
    {
        
        janela.addMouseListener(new MouseListener() 
        {
            @Override
            public void mouseClicked(MouseEvent me) 
            {
                     // tela1.setBackground(Color.RED);
                      tela1.setText(String.valueOf(janela.getMousePosition().getX()));
                      tela2.setText(String.valueOf(janela.getMousePosition().getY()));
                     
            }

            @Override
            public void mousePressed(MouseEvent me) 
            {
             //   texto.setText(String.valueOf(janela.getX()));
                // tela1.setBackground(Color.WHITE);
            }

            @Override
            public void mouseReleased(MouseEvent me) 
            {
                 
                   
            }

            @Override
            public void mouseEntered(MouseEvent me) 
            {
                
                tela3.setText(String.valueOf(janela.getMousePosition().getX()));
                tela4.setText(String.valueOf(janela.getMousePosition().getY()));    
                
            }

            @Override
            public void mouseExited(MouseEvent me)
            {
           //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        tela1.addMouseListener(new MouseListener()
        {
            @Override
            public void mouseClicked(MouseEvent me) 
            {
                      tela1.setBackground(Color.RED);
                      tela1.setForeground(Color.white);
                      
                     
            }

            @Override
            public void mousePressed(MouseEvent me) 
            {
             //   texto.setText(String.valueOf(janela.getX()));
                // tela1.setBackground(Color.WHITE);
            }

            @Override
            public void mouseReleased(MouseEvent me) 
            {
                 
                   
            }

            @Override
            public void mouseEntered(MouseEvent me) 
            {
                
               
                
            }

            @Override
            public void mouseExited(MouseEvent me) 
            {
                //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                tela1.setForeground(Color.black);
                tela1.setBackground(Color.WHITE);
            }
        });
        
         tela2.addMouseListener(new MouseListener() 
         {
            @Override
            public void mouseClicked(MouseEvent me) 
            {
                      tela2.setBackground(Color.RED);
                      tela2.setForeground(Color.white);
                      
                     
            }

            @Override
            public void mousePressed(MouseEvent me) 
            {
             //   texto.setText(String.valueOf(janela.getX()));
                // tela1.setBackground(Color.WHITE);
            }

            @Override
            public void mouseReleased(MouseEvent me) 
            {
                 
                   
            }

            @Override
            public void mouseEntered(MouseEvent me) 
            {
                
               
                
            }

            @Override
            public void mouseExited(MouseEvent me) 
            {
           //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                tela2.setForeground(Color.black);
                tela2.setBackground(Color.WHITE);        
            }
        });
        
          tela3.addMouseListener(new MouseListener() 
          {
            @Override
            public void mouseClicked(MouseEvent me) 
            {
                      tela3.setBackground(Color.RED);
                      tela3.setForeground(Color.white);
                      
                     
            }

            @Override
            public void mousePressed(MouseEvent me) 
            {
             //   texto.setText(String.valueOf(janela.getX()));
                // tela1.setBackground(Color.WHITE);
            }

            @Override
            public void mouseReleased(MouseEvent me) 
            {
                 
                   
            }

            @Override
            public void mouseEntered(MouseEvent me) 
            {
                
               
                
            }

            @Override
            public void mouseExited(MouseEvent me) 
            {
           //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            tela3.setForeground(Color.black);
            tela3.setBackground(Color.WHITE);
            }
        });
          
            tela4.addMouseListener(new MouseListener() 
            {
            @Override
            public void mouseClicked(MouseEvent me) 
            {
                      tela4.setBackground(Color.RED);
                      tela4.setForeground(Color.white);
                      
                     
            }

            @Override
            public void mousePressed(MouseEvent me) 
            {
             //   texto.setText(String.valueOf(janela.getX()));
                // tela1.setBackground(Color.WHITE);
            }

            @Override
            public void mouseReleased(MouseEvent me) 
            {
                 
                   
            }

            @Override
            public void mouseEntered(MouseEvent me) 
            {
                
               
                
            }

            @Override
            public void mouseExited(MouseEvent me) 
            {
           //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            tela4.setForeground(Color.black);
            tela4.setBackground(Color.WHITE);
            }
        });
           
        
    }
}
