
package Aplicacao;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Gui 
{
    JFrame janela;
    
    
    public void construir()
    {

        janela= new JFrame("LP2");
        janela.setBounds(750, 300, 350, 350);
        janela.getContentPane().setLayout(new BorderLayout());
        
        JPanel painelBotoes = new JPanel();
        JButton botaoAbrir= new JButton("abrir");
        
        JTextField text = new JTextField();
        text.setPreferredSize(new Dimension(50,50));
        janela.getContentPane().add(text,BorderLayout.CENTER);
        
        janela.getContentPane().add(painelBotoes,BorderLayout.NORTH);
        painelBotoes.add(botaoAbrir);
      
        JButton botaoMais = new JButton("+");
        janela.getContentPane().add(botaoMais,BorderLayout.WEST);
        
        JButton botaoMenos = new JButton("-");
        janela.getContentPane().add(botaoMenos,BorderLayout.EAST);
        
        JButton sair = new JButton("sair");
        janela.getContentPane().add(sair,BorderLayout.SOUTH);
        
        
        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        janela.setVisible(true);

        
    
    }
    
}
