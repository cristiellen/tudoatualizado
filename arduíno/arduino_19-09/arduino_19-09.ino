int led = 7;
int botao = 10;
boolean auxiliar = false;

void setup() 
{
  pinMode(led,OUTPUT);
  pinMode(botao,INPUT);
}

void loop() {
  if(auxiliar)
  {
    digitalWrite(led,HIGH);  
  }
  else
  {
    digitalWrite(led,LOW);
  }
  if(digitalRead(botao))
  {
    auxiliar =!auxiliar;
    while(digitalRead(botao)){}
  }
}
